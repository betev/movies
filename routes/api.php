<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('movies')->group(function(){
    Route::get('', 'MovieController@index');
    Route::get('{movie}', 'MovieController@show')->where('movie', '[0-9]+');
    Route::put('{movie}', 'MovieController@update')->where('movie', '[0-9]+');
    Route::delete('{movie}', 'MovieController@delete')->where('movie', '[0-9]+');
});

Route::prefix('genres')->group(function(){
    Route::get('', 'GenreController@index');
});

Route::prefix('actors')->group(function(){
    Route::get('', 'ActorController@index');
});
