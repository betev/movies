<?php

namespace App\Models;

/**
 * @OA\Schema(
 *   @OA\Xml(name="Genre"),
 *   @OA\Property(
 *      property="id",
 *      type="integer",
 *      description="Genre ID"
 *  ),
 *   @OA\Property(
 *      property="name",
 *      type="string",
 *      description="Genre name"
 *  )
 * )
 */

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function movies()
    {
        return $this->hasMany('App\Models\Movie');
    }
}
