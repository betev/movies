<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function create(Request $request)
    {
        //
    }

    /**
     * @OA\Get(
     *     path="/api/movies",
     *     tags={"Movie"},
     *     description="Movies",
     *     @OA\Parameter(name="genre_id", description="Genre ID", in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="actor", description="Actor", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response="200", description="Movies",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="integer"),
     *              @OA\Property(property="data", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="genre_id", type="integer"),
     *                      @OA\Property(property="created_at", example="2020-06-15 12:00:00"),
     *                      @OA\Property(property="updated_at", example="2020-06-15 12:00:00"),
     *                      @OA\Property(property="genre", example={"id":1, "name":"Drama"}),
     *                      @OA\Property(property="actors", type="array",
     *                          @OA\Items(
     *                              @OA\Property(property="id", type="integer"),
     *                              @OA\Property(property="fname", type="string"),
     *                              @OA\Property(property="lname", type="string")
     *                          )
     *                      ),
     *                  ),
     *              ),
     *              @OA\Property(property="from", type="integer"),
     *              @OA\Property(property="last_page", type="integer"),
     *              @OA\Property(property="per_page", type="integer"),
     *              @OA\Property(property="to", type="integer"),
     *              @OA\Property(property="total", type="integer"),
     *          )
     *     )
     * )
     */

    public function index(Request $request)
    {
        $query = Movie::query();
        if(request('actor')){
            $query->whereHas('actors', function ($q){
                $q->where('actors.fname', 'like', '%'.request('actor').'%')->orWhere('actors.lname', 'like', '%'.request('actor').'%');
            });
        }
        if(request('genre_id')){
            $query->where('genre_id', request('genre_id'));
        }
        $movies = $query->paginate(request('per_page') ?? 20);
        return $movies;
    }

    /**
     * @OA\Get(
     *     path="/api/movies/{movie}",
     *     tags={"Movie"},
     *     description="Movies",
     *     @OA\Parameter(name="movie", description="Movie ID", in="path", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description="Movies",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="integer"),
     *              @OA\Property(property="genre_id", type="integer"),
     *              @OA\Property(property="created_at", example="2020-06-15 12:00:00"),
     *              @OA\Property(property="updated_at", example="2020-06-15 12:00:00"),
     *              @OA\Property(property="genre", example={"id":1, "name":"Drama"}),
     *              @OA\Property(property="actors", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="fname", type="string"),
     *                      @OA\Property(property="lname", type="string")
     *                  )
     *              ),
     *          )
     *     )
     * )
     */

    public function show(Movie $movie)
    {
        return $movie;
    }

    public function update(Request $request, Movie $movie)
    {
        $movie->genre_id = request('genre_id') ?? $movie->genre_id;
        $movie->title = request('title') ?? $movie->title;
        $movie->save();
    }

    /**
     * @OA\Delete(
     *     path="/api/movies/{movie}",
     *     tags={"Movie"},
     *     description="Movies",
     *     @OA\Parameter(name="movie", description="Movie ID", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description="Movies",
     *          @OA\JsonContent(example={"data":"Success"})
     *     )
     * )
     */

    public function delete(Movie $movie)
    {
        if(!$movie){
            return response(['data' => 'Not found'], 404);
        }
        $movie->delete();
        return response(['data' => 'Success']);
    }
}
