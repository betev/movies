<?php

namespace App\Models;

/**
 * @OA\Schema(
 *   @OA\Xml(name="Movie"),
 *   @OA\Property(
 *      property="id",
 *      type="integer",
 *      description="MovieID"
 *  ),
 *   @OA\Property(
 *      property="genre_id",
 *      type="integer",
 *      description="Genre ID"
 *  ),
 *   @OA\Property(
 *      property="title",
 *      type="string",
 *      description="Movie title"
 *  )
 * )
 */

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{

    use HasFactory;

    protected $with = ['genre', 'actors'];

    public function genre()
    {
        return $this->hasOne('App\Models\Genre', 'id', 'genre_id');
    }

    public function actors()
    {
        return $this->belongsToMany('App\Models\Actor', 'actor_movie', 'movie_id', 'actor_id');
    }
}
