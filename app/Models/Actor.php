<?php

namespace App\Models;

/**
 * @OA\Schema(
 *   @OA\Xml(name="Actor"),
 *   @OA\Property(
 *      property="id",
 *      type="integer",
 *      description="Actor ID"
 *  ),
 *   @OA\Property(
 *      property="fname",
 *      type="string",
 *      description="Actor first name"
 *  ),
 *   @OA\Property(
 *      property="lname",
 *      type="string",
 *      description="Actor last name"
 *  )
 * )
 */

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    use HasFactory;

    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie', 'actor_movies', 'actor_id', 'movie_id');
    }
}
