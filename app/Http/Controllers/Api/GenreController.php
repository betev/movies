<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/genres",
     *     tags={"Genre"},
     *     description="Genres",
     *     @OA\Response(response="200", description="Genres",
     *          @OA\JsonContent(type="array",
     *              @OA\Items(example={"id":1, "name":"Drama"})
     *          )
     *     )
     * )
     */

    public function index()
    {
        return Genre::all();
    }
}
