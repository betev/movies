<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/actors",
     *     tags={"Actor"},
     *     description="Actors",
     *     @OA\Response(response="200", description="Actors",
     *          @OA\JsonContent(type="array",
     *              @OA\Items(example={"id":1, "fname":"Axel", "lname":"F"})
     *          )
     *     )
     * )
     */

    public function index()
    {
        return Actor::all();
    }
}
