<?php

namespace App\Models;


/**
 * @OA\Schema(
 *   @OA\Xml(name="ActorMovie"),
 *   @OA\Property(
 *      property="id",
 *      type="integer",
 *      description="ID"
 *  ),
 *   @OA\Property(
 *      property="movie_id",
 *      type="integer",
 *      description="Movie ID"
 *  ),
 *   @OA\Property(
 *      property="actor_id",
 *      type="integer",
 *      description="Actor ID"
 *  )
 * )
 */

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActorMovie extends Model
{
    use HasFactory;

    protected $table = 'actor_movie';
}
